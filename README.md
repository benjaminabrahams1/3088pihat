# 3088pihat

This microHAT will keep your Pi alive for long enough to shut it down properly. 

To help you decide when to shut the Pi down it would obviously be helpful to know how full your battery is so the HAT will also have a current shunt that will lead to the signal line between 0-3.3V. 
Similarly some status LEDs showing eg Battery Voltage above 80%V to show ‘full’, or just an indicator showing “using battery
power / not” or an indicator warning of low voltage. 
So 3 modules to simulate are:
1. “Voltmeter” or Shunt and signal opamp constraining values to 0-3.3V
2. Power circuitry to convert battery voltage to correct Pi voltages
3. Status LEDs
